#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
MDNSResponder mdns;
const int ledPin = 2;
WiFiServer server(1337);
char ssid[13],cIH[10];
String cI;
int hash[5];
void printWiFiStatus();

void setup(void) {
  Serial.begin(115200);
  if (!mdns.begin("esp", WiFi.localIP())) {
    Serial.println("Error setting up MDNS responder!");
    while(1) { 
      delay(100);
    }
  }
  WiFi.mode(WIFI_AP);
  cI = (String)ESP.getChipId();
  //Hashing
    hash[0]=0;
    hash[1]=0;
    hash[2]=1;
    hash[3]=1;
    hash[4]=0;
    //Hashing 1
      for (int i=0; i<7; i++)
        hash[0] = ( hash[0] + (int)( cI[i] - '0' ) ) %100;
    //Hashing 2
      for (int i=0; i<7; i++)
        hash[1] = ( hash[1] + (int)( cI[i/2] - '0' ) ) %100;
    //Hashing 3
      for (int i=0; i<7; i++)
        hash[2] = ( hash[2] * (int)( cI[i] - '0' ) ) %100;
    //Hashing 4
      for (int i=0; i<7; i++)
        hash[3] = ( hash[3] * (int)( cI[i/2] - '0' ) ) %100;
    //Mash
      hash[4] += hash [0] * 1000000;
      hash[4] += hash [1] * 10000;
      hash[4] += hash [2] * 100;
      hash[4] += hash [3] * 1;
  //!Hashing
  ("Auer " + cI).toCharArray(ssid,13);
  String(hash[4]).toCharArray(cIH, 9);
  Serial.println("");
  Serial.println("Chip : " + (String)ESP.getChipId());
  Serial.println("SSID : " + (String)ssid);
  Serial.println("PSWD : " + (String)cIH);
  WiFi.softAP( ssid ,cIH);
  // Configure GPIO2 as OUTPUT.
  pinMode(ledPin, OUTPUT);

  // Start TCP server.
  server.begin();
}

void loop(void) {
  // Check if module is still connected to WiFi.
  /*
  if (WiFi.status() != WL_CONNECTED) {
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
    }
    // Print the new IP to Serial.
    printWiFiStatus();
  }
  */
  WiFiClient client = server.available();

  if (client) {
    Serial.println("Client connected.");

    while (client.connected()) {
      if (client.available()) {
        char command = client.read();
        if (command == 'H') {
          digitalWrite(ledPin, HIGH);
          Serial.println("LED is now on.");
        }
        else if (command == 'L') {
          digitalWrite(ledPin, LOW);
          Serial.println("LED is now off.");
        }
      }
      else{
        client.println(currentVals());
      }
    }
    Serial.println("Client disconnected.");
    client.stop();
  }
}

void printWiFiStatus() {
  Serial.println("");
  Serial.print("Connected to ");
  //Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

String currentVals(){
  //JSON is a must
  return "{current:{rpm:" + String(60) + ",speed:" + String(90) + ",temp=" + String(70) + ",gear:" + String(3) + "}}";
}

void gearUp(){
  //ServoCrap
}

void gearDown(){
  //ServoCarp
}

